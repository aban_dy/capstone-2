<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \DB::table('statuses')->delete();
       \DB::table('statuses')->insert(array(
        	0 => array(
        		'id' => 1,
        		'name' => 'Pending',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	1 => array(
        		'id' => 2,
        		'name' => 'Approved',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	2 => array(
        		'id' => 3,
        		'name' => 'Declined',
        		'created_at' => null,
        		'updated_at' => null
        	)
        ));
    }
}
