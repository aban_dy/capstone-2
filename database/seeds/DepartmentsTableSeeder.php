<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \DB::table('departments')->delete();
       \DB::table('departments')->insert(array(
        	0 => array(
        		'id' => 1,
        		'name' => 'Armed Guard',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	1 => array(
        		'id' => 2,
        		'name' => 'Perimeter',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	2 => array(
        		'id' => 3,
        		'name' => 'Hotel',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	3 => array(
        		'id' => 5,
        		'name' => 'Traffic',
        		'created_at' => null,
        		'updated_at' => null
        	)
        ));
    }

}
