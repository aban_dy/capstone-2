<?php

use Illuminate\Database\Seeder;

class ReqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('reqs')->delete();
         \DB::table('reqs')->insert(array(
        	0 => array(
        		'id' => 1,
        		'type' => 'Sick Leave',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	1 => array(
        		'id' => 2,
        		'type' => 'Vacation Leave',
        		'created_at' => null,
        		'updated_at' => null
        	)
        ));
    }
}
