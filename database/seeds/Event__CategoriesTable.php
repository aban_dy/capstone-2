<?php

use Illuminate\Database\Seeder;

class Event__CategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \DB::table('event__categories')->delete();
       \DB::table('event__categories')->insert(array(
        	0 => array(
        		'id' => 1,
        		'name' => 'Event',
        		'created_at' => null,
        		'updated_at' => null
        	),
        	1 => array(
        		'id' => 2,
        		'name' => 'Announcement',
        		'created_at' => null,
        		'updated_at' => null
        	)
        ));
    }
}
