<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

    public function category(){
   	return $this->belongsTo('\App\Event_Category');
   }

   public function user(){
   		return $this->belongsTo('App\User');
   }
}
