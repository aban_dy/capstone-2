<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Req extends Model
{
    public function users(){
    	return $this->belongsToMany('\App\User')->withPivot('requested_date','status')->withTimeStamps();
    }
}
