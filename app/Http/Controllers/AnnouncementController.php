<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use Auth;
use Session;

class AnnouncementController extends Controller
{	


	#index
	public function index(){
		$announcements = Announcement::all();
		return view('pages.announcement_all',compact('announcements'));
	}

    #Createing Announcement
	public function create(Request $request){

		$rules = array(
			'body' => 'required',
			'title' => 'required',
		);

		$this->validate($request,$rules);

		$announce = new Announcement;
		$announce->title = $request->title;
		$announce->body = $request->body;
		$announce->category_id = $request->category_id;
		$announce->user_id = Auth::user()->id;
		$announce->save();

		// return redirect()->back();

	}
    #end of creating announcement


    #Deleting Anouncement
	public function destroy($id){
		$anouncement = Announcement::find($id);
		$anouncement->delete();
		return redirect()->back();
	}
    #end of deleting announcement


    #Updating Announcement
    public function updateAnnouncement(Request $req, $id){

    	$announcement = Announcement::find($id);
    	$announcement->body = $req->body;
    	$announcement->title = $req->title;
    	$announcement->save();

    	return redirect()->back();


    }
}
