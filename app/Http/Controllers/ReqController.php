<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;
use App\Req;
use Session;
use Carbon\Carbon;
class ReqController extends Controller
{


    
	public function index(){

		$user = User::find(Auth::user()->id);
		$reqs = Req::all();
		// For employee view
		return view('pages.personal_requests',compact('user','reqs'));

	}

	public function showAllRequests(){

		//For admin view
		return view('pages.requests');
	}


	/*
		Creating Request
	*/
		public function createRequest(Request $req){

			$this->validate($req,[
				"date" => "required"
			]);
			if($req->date){

				$userId = Auth::user()->id;
				$request = Req::find($req->request_id);
				$request->users()->attach($userId,[
					"req_id" => $req->request_id,
					"requested_date" => $req->date
				]);

				Session::flash('message','success!');

			}else{
				Session::flash('message','Date Required!');
			}

			return redirect()->back();
		}

	/*
		Deleting Request
	*/

		public function deleteRequest(Request $req){
			$request = Req::find($req->id);
			$userId = Auth::user()->id;
			$request->users()->detach($userId);

			return redirect()->back();

		}


	/*
		Updating Request
	*/	
		public function updateRequest(Request $req, $id){
			


			if($req->date){
				$update = Req::find($id);
				$update->users()->updateExistingPivot(Auth::user()->id,[
					"req_id" => $req->request_id,
					"requested_date" => $req->date
				]);
				Session::flash('message','success!');
			
			}else{
				Session::flash('message','Date Required!');
			}

			
			return redirect()->back();
		}



	/*
		Deleting all request
	*/
		public function deleteAllRequest(){
			$deletes = Req::all();

			foreach($deletes as $delete){
				$delete->users()->detach(Auth::user()->id);
			}
			return redirect()->back();

		}	

	/*
		Changing Request's status
	*/



}
