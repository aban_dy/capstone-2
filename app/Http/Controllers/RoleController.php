<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Announcement;
use Hash;
use Session;
use Illuminate\Support\Facades\Storage;
use Auth;

class RoleController extends Controller
{
   public function create(Request $request){

   	$users = User::where('email',$request->email)->first();
   	$message = "";


   	if($users){
   		$message = "Email already taken";
   	}else{
   		$employee = new User;
   		$employee->name = $request->name;
   		$employee->email = $request->email;
   		$employee->password = Hash::make($request->password);
         $employee->department_id = $request->department;
   		$employee->save();

   		$message = "Employee Added!";
         
   	}
      
   	Session::flash('message','Employee Added');
   	return redirect()->back()->with('message',$message);

   }


   #search
   public function search(Request $req){
   		$output = "";
   		$employees = User::where('name','LIKE','%'.$req->name.'%')->get();

   		if($employees){
   			foreach ($employees as $employee) {
   				$output = '<tr>'.
   				'<td>' . $employee->name . '</td>'.
   				'<td>' . $employee->department->name . '</td>'.
   				'<td>' . 'present' . '</td>'.
   				'<td>
   				<a href="#'. $employee->id .'">
   					<i class="fas fa-trash-alt text-danger px-2"></i>
   				</a>
   				<a href="#">
   					<i class="fas fa-pencil-alt text-warning px-2"></i>
   				</a>
   				</td>' .
   				'</tr>';
   			}
   		}

   		return response($output);
   }


   #Delete Employee
   #User soft delete
   public function deleteEmployee($id){
   		$employee = User::find($id);
         $users = Announcement::where('user_id', $id)->get();

         foreach($users as $user){
            $user->delete();
         }

   		$employee->delete();
   		return redirect()->back();
   }


   #Update Employee
   public function updateEmployee($id, Request $req){
      $employee = User::find($id);
      $employee->name = $req->updated_name;
      $employee->department_id = $req->updated_department;



      $employee->save();
      return redirect()->back();
   }


   #Profile upload
   public function updateProfile($id,Request $req){
      $user = User::find($id);

      $this->validate($req,[
         "name" => "required",
         "email" => "required",
         "password" => "required",
         "image" => "image|max:5000"
      ]);


      
      $user->name = $req->name;
      $user->email = $req->email;
      

      if($req->hasFile('image')){
           
               Storage::delete('img/'.Auth::user()->imgName);
               $avatar = $req->file('image');
               $avate_new_name = time(). "." .$avatar->getClientOriginalExtension();
               $avatar->move('img/', $avate_new_name);
               $user->imgName = $avate_new_name;
      }

           
      if($req->has('password')){

         $user->password = Hash::make($req->password);

      }

      $user->save();

      Session::flash('message','Success!');
      return redirect()->back();
   }
}
