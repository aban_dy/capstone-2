<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{	

    public function index(){
    	$attendances = Attendance::whereDate('started_at',Carbon::today())->groupBy('user_id')->paginate(10);
     

    	return view('pages.attendance',compact('attendances'));
    }

    

    public function show($id){

        $attendances = Attendance::where('user_id',$id)->get();

        return view('pages.personal_attendance',compact('attendances'));
    }


    /*
    Employee start and stop attendance
    */
    
    public function start($userId){

    	// $user = Attendance::where('user_id',$userId)->first();
        $user = Attendance::where('user_id',$userId)->whereNull('ended_at')->first();

        if($user){

    		// if($user->created_at->format('M-d-Y') == Carbon::now()->format('M-d-Y')){
    		// 	Session::flash('message','Cannot start on the same day!');
    		// 	return redirect()->back();
    		// }

            Session::flash('message','Already started!');
            return redirect()->back();


        }else{
           $attendance = new Attendance;
           $attendance->user_id = $userId;
           $attendance->isPresent = 1;
           $attendance->started_at = Carbon::now();
           $attendance->save();

           Session::flash('message','Success!');  
       }

       return redirect()->back();
       
   }

   public function stop($userId){
       $attendance = Attendance::where('user_id',$userId)->whereNull('ended_at')->first();	

       if($attendance){
          $attendance->ended_at = Carbon::now();
          $attendance->save();
          Session::flash('message',strval('Ended at ' . $attendance->ended_at->format('M-d-Y g:i a')));
          return redirect()->back();
    		// ->format('M-d-Y g:i a')
      }else{
          Session::flash('message','Oops! Something is not right. :(');
          return redirect()->back();
      }	

      
  }

    /*
    Deleting and Updating Attendance
    */

    public function deleteAttendance($id){

        $deletes = Attendance::where('user_id',$id)->get();

        foreach($deletes as $delete){
            $delete->delete();
        }
        

        return redirect()->back();

    }
    
    public function deleteAll(){
        $deletes = Attendance::all();
        
        if($deletes){
            foreach($deletes as $delete){
                $delete->delete();
            }
            Session::flash('message','Cleared!');
            return redirect()->back();
        }
    }



}
