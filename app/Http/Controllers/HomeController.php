<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event_Category;
use App\Announcement;
use App\User;
use App\Department;
use App\Attendance;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $attendances = Attendance::where('isPresent',1)->get();
        $users = User::whereNull('deleted_at')->limit(12)->get();
        $categories = Event_Category::all();
        // $announcements = User::whereNull('deleted_at')->get();
        $announcements = Announcement::orderBy('created_at','desc')->get();
        return view('home',compact('categories','announcements','users','attendances'));
    }

    public function employees(){

        $employees = User::where('role_id',2)->paginate(10);
        $positions = Department::all();


        return view('pages.employees',compact('employees','positions'));

    }



    #Profile
    public function show($id){
        $user = User::find($id);
        return view('pages.profile',compact('user'));
    }


}
