


// Create Annoucnement

//end of announement


//SWAL
const deleteAlert = (event) => {

	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			document.querySelector('.delete').submit();
			Swal.fire({
				icon : 'success',
				title : 'Success!',
				showConfirmButton : false
			});
			window.location.href = "#history";
		}
	})

}



const deleteEmployee = (id) => {

	let submitDelete = document.forms['delete_employee_'+id];

	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			submitDelete.submit();
			Swal.fire({
				icon : 'success',
				title : 'Success!',
				showConfirmButton : false
			});
			window.location.href = "#delete_employee"+id;
		}
	})

}



//Attendance
const start = () => {

	Swal.fire({
		title: 'Are you sure?',
		icon: 'info',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ok'
	}).then((result) => {
		if (result.value) {
			document.forms.namedItem('start').submit();
		}
	});

	


}

const stop = () => {

	Swal.fire({
		title: 'Are you sure?',
		icon: 'info',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ok'
	}).then((result) => {
		if (result.value) {
			document.forms.namedItem('stop').submit();
		}
	})



}


