<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
Custom Routes
*/
Route::middleware('admin')->group(function(){


#Anouncement Routes
Route::post('/announce','AnnouncementController@create')->name('create');
Route::delete('/delete/{id}','AnnouncementController@destroy');
Route::get('/events','AnnouncementController@index')->name('events');
Route::patch('/updateAnnouncement/{id}','AnnouncementController@updateAnnouncement');


#Employee Route
Route::get('/employees', 'HomeController@employees')->name('employees');
Route::post('/registerEmployee','RoleController@create')->name('registerEmployee');
Route::post('/search','RoleController@search');
Route::delete('/deleteEmployee/{id}','RoleController@deleteEmployee')->name('deleteEmployee');
Route::patch('/updateEmployee/{id}','RoleController@updateEmployee');


#Attendance Route
Route::get('/attendance','AttendanceController@index')->name('attendance');
Route::get('/attendance/{id}','AttendanceController@show');
Route::delete('/deleteAttendance/{id}','AttendanceController@deleteAttendance');
Route::get('/deleteAll','AttendanceController@deleteAll');

});

Route::post('/start/{userId}','AttendanceController@start');
Route::post('/stop/{userId}','AttendanceController@stop');

#Profile Route
Route::get('/profile/{id}','HomeController@show')->name('profile');
Route::post('/updateProfile/{id}','RoleController@updateProfile');

#Requests Routes
Route::get('/requests','ReqController@index')->name('requests');
Route::get('/deleteAllRequest','ReqController@deleteAllRequest');
Route::post('/createRequest','ReqController@createRequest');
Route::patch('/updateRequest/{id}','ReqController@updateRequest');
Route::delete('/deleteRequest/{id}','ReqController@deleteRequest');

#Default routes
Route::get('/', function () {
    return view('home');
})->middleware('auth');
Auth::routes();
Route::get('/', 'HomeController@index');
#end of default routes





