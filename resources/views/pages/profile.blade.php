@extends('layouts.master')

@include('includes.navbar')
@section('content')

<div class="container-fluid m-0">
	<div class="row">
		<div class="col-lg-9 ml ml-auto bg-wrapper">
			<span class="bg-2 d-none d-lg-block">
				<img src="{{asset('img/profile-bg.svg')}}" class="w-100 m-0 py-3">
			</span>
			<div class="row align-items-center text-black-50 p-3">
				<div class="col-lg-4 col-md-12 col-sm-9">
					<div class="img-fluid text-center">
						<img src="{{asset('img/'.$user->imgName)}}" class="rounded-circle" width="200px" height="200px">
						<form class="d-none">
							<input type="file" name="" class="btn btn-info">
						</form>
					</div>
				</div>

				<div class="col-lg-8 col-md-12 col-sm-9 text-center text-lg-left my-2">
					<h3 class="text-uppercase text-fluid">{{$user->name}}</h3>
					<h4 class="text-uppercase">{{$user->department->name}}{{__('  Department')}}</h4>
					<strong>{{$user->email}}</strong>
					<div class="d-block align-items-center w-100">
						@auth
						@if(Auth::user()->id == $user->id)
						<button class="btn btn-sm btn-primary my-2 text-right" data-toggle="modal" data-target="#edit_profile">
							<span>{{__('Edit profile')}}</span>
							<i class="fas fa-cog"></i>
						</button>
						@endif
					@endauth
					</div>
				</div>
			</div>
		</div>
	</div>

</div>




{{-- Edit profile modal --}}
<div class="modal" tabindex="-1" role="dialog" id="edit_profile">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">{{__('Edit Profile')}}</h5>
			</div>
			<div class="modal-body">
					<form action="/updateProfile/{{Auth::user()->id}}" method="POST" enctype="multipart/form-data" id="updateProfile">
					@csrf
					<div class="img-fluid text-center">
						<img src="{{asset('img/'. Auth::user()->imgName)}}" id="profileImg" height="100px" width="100px" class="rounded-circle">
					</div>
					<div class="form-group">
						<label>{{__('Name')}}</label>
						<input type="text" name="name" placeholder="enter name..."class="form-control" value="{{Auth::user()->name}}">
					</div>
					<div class="form-group">
						<label>{{__('email')}}</label>
						<input type="text" name="email" placeholder="enter name..."class="form-control" value="{{Auth::user()->email}}">
					</div>
					<div class="form-group">
						<label>{{__('password')}}</label>
						<input type="password" name="password" class="form-control" value="">
					</div>
					<div class="form-group">
						<label>{{__('Profile Image')}}</label>
						<input type="file" name="image" class="form-control-file">
					</div>
				</form>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="updateProfile()">Save changes</button>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
	const updateProfile = () => {
		document.forms.namedItem('updateProfile').submit();
	}
</script>





@endsection
