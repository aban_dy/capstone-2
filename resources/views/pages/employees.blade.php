@extends('layouts.master')
@section('content')
@include('includes.navbar')



<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9 ml-auto p-2">

			<div class="w-100 p-2">
				<div class="card-header d-flex align-items-center justify-content-between">
					<h5 class="cart-title text-black-50 p-2 m-0">{{ __('Employees') }}</h5>
					<div class="form-group m-0 w-50 d-flex align-items-center">
						<form class="w-100">
						{{csrf_field()}}
						<input type="search" name="seach" class="form-control" placeholder="Search employee" id="search">
						</form>
						<i class="fas fa-search text-black-50 fa-lg p-2"></i>
					</div>
					<span class="align-items-center">
						<button class="btn d-flex align-items-center nav-link" data-toggle="modal" data-target="#add__employee">
							<i class="fas fa-user-plus text-success"></i>
						</button>
					</span>
				</div>
			</div>
			<hr>

			<div class="p-2" id="__attendance">
				<div class="w-100">
					
					<table class="table table-border" >
						<thead class="text-uppercase">
							<th>{{ __('Name') }}</th>
							<th>{{ __('Department') }}</th>
							<th>{{ __('Option') }}</th>
						</thead>

						{{-- Fetch --}}
						<tbody id="table" class="text-black-50">
						{{-- here --}}
						</tbody>


						<tbody class="text-black-50 " id="initial_table">
							@foreach($employees as $employee) 
							<tr>
								<td>
								<span id="old_name_{{$employee->id}}">
									{{$employee->name}}
								</span>
								<form class="form-group w-100" id="new_details_{{$employee->id}}" action="/updateEmployee/{{$employee->id}}" method="POST">
									@csrf
									@method('PATCH')
									<input type="text" name="updated_name" class="form-control d-none" value="{{$employee->name}}" id="new_name_{{$employee->id}}">
									<button class="btn btn-sm btn-success my-2 d-none" type="button" onclick="submitUpdate({{$employee->id}})" id="submit_button_{{$employee->id}}">
										<i class="fas fa-check"></i>
									</button>
									<button class="btn btn-sm btn-primary d-none" type="button" onclick="cancelUpdate({{$employee->id}})" id="cancel_button_{{$employee->id}}">
										<i class="fas fa-minus"></i>
									</button>
								</td>
								<td>
									<span id="old_department_{{$employee->id}}">
										{{$employee->department->name}}
									</span>
									<select class="form-control d-none" name="updated_department" id="new_department_{{$employee->id}}">
										@foreach($positions as $department)
											<option value="{{$department->id}}">
												{{$department->name}}
											</option>
										@endforeach
									</select>
								</form>
								</td>
								<td>
									<form action="/deleteEmployee/{{$employee->id}}" method="POST" class="d-flex" id="delete_employee_{{$employee->id}}">
			
										@csrf
										@method('DELETE')
										<button class="btn btn-sm" type="button" onclick="deleteEmployee({{$employee->id}})">
											<i class="fas fa-trash-alt text-danger"></i>
										</button>
										<button class="btn btn-sm" type="button" onclick="update({{$employee->id}})">
											<i class="fas fa-pencil-alt text-warning"></i>
										</button>
										<a class="btn btn-sm" href="/profile/{{$employee->id}}">
											<i class="fas fa-user text-pikachu"></i>
										</a>
									</form>
								</td>
							</tr>
							
							@endforeach
						</tbody>
					</table>
					{{-- Pagination --}}
					{{$employees->links()}}	
				</div>
			</div>

		</div>
	</div>

</div>

{{-- Add employee modal --}}
<div class="modal fade" id="add__employee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h4 class="text-center text-black-50">
					Register Employee
				</h4>
			</div>
			{{-- inster form here --}}

			<div class="row justify-content-center p-3">
				<div class="col-md-8 col-lg-9 text-black-50">

					<form id="register-form" action="{{route('registerEmployee')}}" method="POST">
						{{csrf_field()}}
						@csrf
						@if(isset($message))
							<strong class="alert text-black-50">{{$message}}</strong>
						@endif
						<div class="form-group"  >
							<label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>

							<div class="w-100">
								<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

								@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

							<div class="w-100">
								<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>

						<div class="form-group">
							<label for="department" class="col-form-label text-md-right">{{ __('Department') }}</label>

							<div class="w-100">
								<select class="form-control" name="department">
									@foreach($positions as $department)
									<option value="{{$department->id}}">{{$department->name}}</option>
									@endforeach
								</select>

								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>

							<div class="w-100">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>


					</div>
				</div>

				{{-- end of form --}}
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="nav-link btn btn-success" type="submit" onclick = "register()">
					<span class="text-white">{{ __('Register Employee') }}</span>
					</button>
			</form>
		</div>
	</div>
{{-- end of add employee modal --}}




<script type="text/javascript">

	const search = document.getElementById('search');
	const table = document.getElementById('table');
	const initTable = document.getElementById('initial_table');

	//Search
	search.addEventListener('input',()=>{

		if(search.value != ""){
			initial_table.classList.add('d-none');
			table.classList.remove('d-none');
			if (document.querySelector('.pagination')){
				document.querySelector('.pagination').classList.add('d-none');
			}
			let data = new FormData;
			data.append('_token' , '{{csrf_token()}}');
			data.append('name', search.value);

			fetch('/search',{
				method: "POST",
				body: data
			}).then(res=>res.text())
			.then(json=>{
				table.innerHTML = json;
				console.log(json);
				
			});

		}else{
			initial_table.classList.remove('d-none');
			table.classList.add('d-none');
			if(document.querySelector('.pagination')){
				document.querySelector('.pagination').classList.remove('d-none');
			}

		}

	

	//end of search
	});


	//Register
	document.getElementById('register-form').addEventListener('submit',()=>{
		Swal.fire({
			title: 'success!'
		})
	});
	//Update

	const update = (id) => {

		document.getElementById('old_name_'+id).classList.add('d-none');
		document.getElementById('old_department_'+id).classList.add('d-none');

		document.getElementById('new_name_'+id).classList.remove('d-none');
		document.getElementById('new_department_'+id).classList.remove('d-none');
		document.getElementById('submit_button_'+id).classList.remove('d-none');
		document.getElementById('cancel_button_'+id).classList.remove('d-none');
	}

	const cancelUpdate = (id) => {
		document.getElementById('old_name_'+id).classList.remove('d-none');
		document.getElementById('old_department_'+id).classList.remove('d-none');

		document.getElementById('new_name_'+id).classList.add('d-none');
		document.getElementById('new_department_'+id).classList.add('d-none');
		document.getElementById('submit_button_'+id).classList.add('d-none');
		document.getElementById('cancel_button_'+id).classList.add('d-none');
	}


	const submitUpdate = (id)=>{

		//Get the forms
		
		let submit = document.forms.namedItem('new_details_'+id).submit();




		//Add classes to forms and table row
		document.getElementById('old_name_'+id).classList.remove('d-none');
		document.getElementById('old_department_'+id).classList.remove('d-none');

		document.getElementById('new_name_'+id).classList.add('d-none');
		document.getElementById('new_department_'+id).classList.add('d-none');
		document.getElementById('submit_button_'+id).classList.add('d-none');
		document.getElementById('cancel_button_'+id).classList.add('d-none');

		Swal.fire({
			title: 'success!'
		});

		// //reload
		// location.reload();
	}



	//end of update function

	</script>



	@endsection