@extends('layouts.master')
@section('content')
@include('includes.navbar');

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9 ml-auto col-sm-12">
			<div class="w-100 p-2">
				<div class="card-header d-flex align-items-center justify-content-between">
					<h5 class="cart-title text-black-50 p-2 m-0">{{ __('Attendance') }}
					</h5>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="container-fluid">
	<dir class="row">
		<div class="col-lg-9 ml-auto col-sm-12">
			<strong class="text-black-50 h3 d-block">{{Carbon\Carbon::now()->format('M d Y')}}</strong>
			<button class="btn btn-info shadow-sm p-2" type="button" onclick="clearAll()">Clear All</button>
			
			<table class="table table-border p-2">
				<thead>
					<th>{{__('Name')}}</th>
					<th>{{__('Attendance Status')}}</th>
					<th>{{__('Started At')}}</th>
					<th>{{__('Ended At')}}</th>
					<th>{{__('Action')}}</th>
				</thead>
				<tbody class="text-black-50" id="table2">
					{{-- Fetch --}}
				</tbody>
				<tbody class="text-black-50" id="initial_table2">
					@foreach($attendances as $attendance)
					<tr>
						<td>{{$attendance->user->name}}</td>
						<td>{{$attendance->isPresent == 0 ? "Absent" : "Present"}}</td>
						<td>{{$attendance->started_at}}</td>
						<td>{{$attendance->ended_at}}</td>
						<td>
							<a class="btn btn-sm btn-secondary" href="/profile/{{$attendance->user->id}}">{{__('Profile')}}
							</a>
							<button class="btn btn-sm btn-danger" onclick="deleteAttendance({{$attendance->user->id}})" type="button">
								{{__('Delete')}}
							</button>

							{{-- Hidden Forms --}}
							<form class="d-none" method="POST" action="/deleteAttendance/{{$attendance->user->id}}" id="user_{{$attendance->user->id}}">
								@csrf
								@method('DELETE')
							</form>


						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	const deleteAttendance = (id) => {

		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				document.forms.namedItem('user_'+id).submit();
				Swal.fire({
					icon : 'success',
					title : 'Success!',
					showConfirmButton : false
				});
				
			}
		});

	}

	const clearAll = () => {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				window.location.href = "/deleteAll"
				Swal.fire({
					icon : 'success',
					title : 'Success!',
					showConfirmButton : false
				});
				
			}
		});
	}

</script>


@endsection

