@extends('layouts.master')
@section('content')
@include('includes.navbar')
<div class="col-sm-12 col-mg-6 col-lg-9 ml-auto p-3 shadow-lg" >
    <div class="card py-3 shadow bg-secondary w-100">

        <div class="card-header border-bottom text-center">
            <span class="card-title text-white-50 h5" id="history">
                <i class="fas fa-calendar-check fa-lg text-warning p-2"></i>
                {{ __('Annoucements / Events') }}
            </span>
        </div>

        <div class="card-body vh-100" >
            @foreach($announcements as $announcement)
            <div class="card bg-pikachu mt-2">
                <div class="card-body">
                    <div class="d-flex align-items-start">
                        <div class="card-text text-right">
                           <img src="{{asset('img/'.$announcement->user->imgName)}}" width="64px" height="64px" class="bg-secondary rounded-circle">
                           <p class="text-white-50 m-0">
                            {{$announcement->user->email}}
                        </p>
                        <p class="text-white-50 m-0">#{{$announcement->category->name}}</p>
                        
                        <p class="text-white-50 m-0">{{$announcement->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="cart-text px-3 text-capitalize w-100">
                        <strong class="text-white-50 h3 my-5" id="old_title_{{$announcement->id}}">#{{$announcement->title}}</strong>
                        <p class="text-white-50" id="old_body_{{$announcement->id}}">{{$announcement->body}}</p>
                        {{-- Hidden form --}}
                        <form class="form-group w-50 d-none" id="update_announcement_{{$announcement->id}}"
                            method="POST" action="updateAnnouncement/{{$announcement->id}}" 
                            >
                            @csrf
                            @method('PATCH')
                            <input type="text" name="title" value="{{$announcement->title}}" class="form-control bg-pikachu text-white-50">
                            <textarea class="form-control w-100 bg-pikachu text-white-50" name="body"></textarea>
                        </form>

                    </div>
                </div>    
            </div>
            <div class="card-footer">
                <form class="d-none" method="POST" action="/delete/{{$announcement->id}}" id="announcement_{{$announcement->id}}">
                    @csrf
                    @method('DELETE')
                </form>
                <button class="btn btn-sm btn-danger" onclick="deleteAnnouncement({{$announcement->id}})">Delete</button>
                @if(Auth::user()->id == $announcement->user->id)
                <button class="btn btn-sm btn-warning" onclick="updateAnnouncement({{$announcement->id}})" id="update_button_{{$announcement->id}}">Update</button>
                @endif
            </div>
        </div>
        @endforeach
    </div>



 

<script type="text/javascript"> 
        //Delete specific announcement
        const deleteAnnouncement = (id) => {
            let url = id;
            Swal.fire({
                title: 'Are you sure?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok'
            }).then((result) => {
                if (result.value) {
                    document.forms.namedItem('announcement_'+url).submit();
                }
            });


        }
        

        //Updated Announcement
        const updateAnnouncement = (id) => {
            let url = id;
            document.getElementById('old_title_'+id).classList.add('d-none');
            document.getElementById('old_body_'+id).classList.add('d-none');
            document.getElementById('update_announcement_'+id).classList.remove('d-none');


            document.getElementById('update_button_'+id).addEventListener('click',function(){

                Swal.fire({
                    title: 'Are you sure?',
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                    if (result.value) {
                        document.forms.namedItem('update_announcement_'+url).submit();
                    }
                });
            });

        }

        /*
        Swal.fire({
            title: 'Are you sure?',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok'
        }).then((result) => {
            if (result.value) {
                document.forms.namedItem('update_announcement_'+url).submit();
            }
        });*/

        

</script>

    @endsection