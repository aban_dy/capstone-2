@extends('layouts.master')
@section('content')
@include('includes.navbar')

<div class="container">
	<div class="row">
		<div class="col-lg-9 ml-auto">
			<div class="row">
				<h3 class="text-black-50 p-3 d-block">{{__('Requests')}}</h3>
				
				
				<div class="col-lg-12 p-3">
					<button class="btn btn-info shadow-sm p-2" type="button" onclick="clearRequests()">Clear All</button>
					<table class="table table-border">
						<thead>
							<th>Type</th>
							<th>Date</th>
							<th>Status</th>
							<th></th>
						</thead>
						<tbody>
							<td></td>
							<td></td>
							<td></td>
						</tbody>
					</table>
				</div>

				
			</div>
		</div>
	</div>
</div>

@endsection