@extends('layouts.master')
@section('content')
@include('includes.navbar')


{{-- Table View --}}
<div class="container">
	<div class="row">
		<div class="col-lg-9 ml-auto">
			<div class="row">
				<h3 class="text-black-50 p-3 d-block">{{__('Requests')}}</h3>
				<div class="col-lg-12 p-3">
					<button class="btn btn-info shadow-sm p-2" type="button" onclick="clearAllRequest()">Clear All</button>
					<table class="table table-border">
						<thead>
							<th>Type</th>
							<th>Date Requested</th>
							<th>Status</th>
							<th></th>
						</thead>
						<tbody>
							@foreach($user->requests as $request)
							<tr>
								<td>{{$request->type}}</td>
								<td>{{\Carbon\Carbon::parse($request->pivot->requested_date)->format('M-d-Y')}}</td>
								<td>{{$request->pivot->status}}</td>
								<td>
									<form action="/deleteRequest/{{$request->id}}" class="d-none" id="deleteRequest_{{$request->id}}" method="POST">
										@csrf
										@method('DELETE')
									</form>
									<button class="btn btn-primary btn-sm" type="button" onclick="deleteRequest({{$request->id}})">
										Delete
									</button> 
									<button class="btn btn-pikachu btn-sm" data-toggle="modal" data-target="#update_request_{{$request->id}}">
										Update
									</button>
								</td>
							</tr>
							{{-- Update Requests Modal --}}
							<div class="modal fade" id="update_request_{{$request->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">Edit Request</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											{{-- Form Here --}}
											<div class="form-group">
												<form action="/updateRequest/{{$request->id}}" method="POST">
													@csrf
													@method('PATCH')
													<div class="form-group">
														<label class="text-black-50">{{__('Choose Request')}}</label>
														<select name="request_id" class="form-control">
															@foreach($reqs as $req)
															<option class="form-control" value="{{$req->id}}">
																{{$req->type}}
															</option>
															@endforeach
														</select>
													</div>
													<div class="form-group">
														<label class="text-black-50">{{__('Choose Date')}}</label>
														<input type="date" name="date" min="{{Carbon\Carbon::now()}}" class="form-control" value="{{$request->requested_date}}">
													</div>

											</div>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Save changes</button>
										</div>
										</form>
									</div>
								</div>
							</div>


							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	//delete on request
	const deleteRequest = (id) => {
		let url = id;
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				document.forms.namedItem('deleteRequest_'+url).submit();
				Swal.fire({
					icon : 'success',
					title : 'Success!',
					showConfirmButton : false
				});
				
			}
		});

	}

	//delete all request
	const clearAllRequest = () => {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				window.location.href = "/deleteAllRequest"
				Swal.fire({
					icon : 'success',
					title : 'Success!',
					showConfirmButton : false
				});
				
			}
		});
	}
</script>

{{-- Create Requests --}}
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9 ml-auto p-3">
			<div class="row">
				{{-- Request From --}}
				<div class="col-lg-12">
					<div class="card shadow w-100">
						<div class="card-header">
							<h3 class="text-center">Create Request</h3>
						</div>
						<div class="card-body">
							{{-- Request Form --}}
							<div class="form-group">
								<form action="/createRequest" method="POST">
									@csrf
									<div class="form-group">
										<label class="text-black-50">{{__('Choose Request')}}</label>
										<select name="request_id" class="form-control">
											@foreach($reqs as $req)
											<option class="form-control" value="{{$req->id}}">
												{{$req->type}}
											</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label class="text-black-50">{{__('Choose Date')}}</label>
										<input type="date" name="date" min="{{Carbon\Carbon::now()}}" class="form-control">
									</div>
									<button class="btn btn-pikachu btn-sm" type="submit">
										Submit
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





@endsection