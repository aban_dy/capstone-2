@extends('layouts.master')
@section('content')
@include('includes.navbar');

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-9 ml-auto w-100 col-sm-12">
			<div class="row">

				<div class="col-sm-3 w-100 col-lg-12">
					<div class="w-100 p-2">
						<div class="card-header d-flex align-items-center justify-content-between">
							<h5 class="cart-title text-black-50 p-2 m-0">{{ __('Attendance') }}
							</h5>
						</div>
					</div>
				</div>

				<div class="col-sm-3 w-100 col-lg-12">

						<strong class="text-black-50 p-2">{{Carbon\Carbon::now()->format('M d Y')}}</strong>
						<table class="table table-border p-2">
							<thead>
								<th>{{__('Name')}}</th>
								<th>{{__('Date')}}</th>
								<th>{{__('Started At')}}</th>
								<th>{{__('Ended At')}}</th>
							</thead>
							<tbody class="text-black-50" id="initial_table2">
								@foreach($attendances as $attendance)
								<tr>
								<td>{{$attendance->user->name}}</td>
								<td>{{$attendance->created_at->format('M-d-Y')}}</td>
								<td>{{$attendance->started_at}}</td>
								<td>{{$attendance->ended_at}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>

				</div>

			</div>
		</div>
	</div>
</div>


 
{{-- 
@foreach($users as $user)
	@foreach($user->requests as $request)
	
	@endforeach
@endforeach  
--}}





@endsection


