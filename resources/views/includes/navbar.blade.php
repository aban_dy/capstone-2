
{{-- Nav --}}
<nav class="nav navbar-light d-none d-lg-block">
    {{-- Sidenav --}}
    <div class="row">

        <div class="col-lg-3 vh-100 fixed-top bg-white shadow overflow-auto">
            
            <div class="navbar-header text-center">
                <a href="/" class="navbar-brad text-uppercase nav-link font-weight-bold text-primary h4 text-center py-3">{{config('app.name','POLSTR')}}
                </a>
                <hr>
                <img src="{{asset('img/'.Auth::user()->imgName)}}" class="rounded-circle text-center" height="64px" width="64px">
                <p class="text-black-50 h4 py-2">Hello! {{Auth::user()->name}}</p>
                <hr>
            </div>
            <ul class="nav w-100 d-flex flex-column">
                {{-- Employee concerns --}}
                @auth
                @if(Auth::user()->role_id == 2)
                <li class="nav-item shadow-sm w-100 d-flex justify-content-center">
                    <form class="d-none" action="/start/{{Auth::user()->id}}" method="POST" id="start">
                        @csrf
                    </form>
                     <form class="d-none" action="/stop/{{Auth::user()->id}}" method="POST" id="stop">
                        @csrf
                    </form>
                    <button type="button" class="nav-link p-3 align-items-center d-flex justify-content-between btn btn-success btn-sm m-2" onclick="start()">
                        <span class="w-100 text-center" id="span_text">
                            {{__('Start shift')}}
                        </span>
                    </button>
                    <button type="button" class="nav-link p-3 align-items-center d-flex justify-content-between btn btn-danger btn-sm m-2" onclick="stop()">
                        <span class="w-100 text-center" id="span_text">
                            {{__('Stop shift')}}
                        </span>
                    </button>
                </li>
                <li class="nav-item shadow-sm w-100">
                    <a href="/" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Announcements/Events</span>
                        <i class="fas fa-calendar-check"></i>
                    </a>
                </li>
                <li class="nav-item shadow-sm w-100">
                    <a href="/attendance/{{Auth::user()->id}}" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Attendance</span>
                        <i class="fas fa-id-card"></i>
                    </a>
                </li>
                <li class="nav-item shadow-sm w-100 text-center">
                    <a href="{{route('requests')}}" class="nav-link p-3">
                        {{__('Requests')}}
                    </a>
                </li>
                
                {{-- End of employee concerns --}}
                @else
                {{-- Admin concerns --}}
                <li class="nav-item shadow-sm w-100">
                    <a href="/" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Dashboard</span>
                        <i class="fas fa-tachometer-alt"></i>
                    </a>
                </li>

                <li class="nav-item shadow-sm w-100">
                    <a href="{{route('employees')}}" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Employees</span>
                        <i class="fas fa-users"></i>
                    </a>
                </li>
                <li class="nav-item shadow-sm w-100">
                    <a href="{{route('attendance')}}" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Attendance</span>
                        <i class="fas fa-id-card"></i>
                    </a>
                </li>
                <li class="nav-item shadow-sm w-100">
                    <a href="{{route('events')}}" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Announcements/Events</span>
                        <i class="fas fa-calendar-check"></i>
                    </a>
                </li>
                <li class="nav-item shadow-sm w-100">
                    <a href="{{route('requests')}}" class="nav-link p-3 align-items-center d-flex justify-content-between">
                        <span class="text-black-50">Requests</span>
                        <i class="fas fa-calendar"></i>
                    </a>
                </li>
            </ul>
            @endif
            @endauth
            {{-- End of admin --}}
            <div class="mt-auto">
               <div class="nav-item w-100 p-2">
                <a href="/profile/{{Auth::user()->id}}" class="nav-link btn btn-secondary align-items-center d-flex justify-content-between shadow">
                    <span class="text-white-50">Profile</span>
                    <i class="fas fa-cogs"></i>
                </a>
            </div>
            <div class="nav-item w-100 p-2">
                
                <button type="button" class="w-100 btn btn-primary align-items-center d-flex justify-content-between shadow" data-toggle="modal" data-target="#exampleModal">
                 <span class="text-white-50">{{ __('Logout') }}</span>
                 <i class="fas fa-sign-out-alt"></i>
                 </button>
                 
            </div>
         </div>
{{-- end of sidenav --}}
</nav>
{{-- end of nav --}}



