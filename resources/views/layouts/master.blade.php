<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'POLSTR') }}</title>

    <!-- Scripts -->
    
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/custom.js') }}" defer></script>
    <!-- {-- Fontawesome --}} -->
    <script src="https://kit.fontawesome.com/dda444c3f4.js" crossorigin="anonymous" defer></script>
    {{-- Chart js --}}
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js" defer></script> 
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class="bg-info">

    <main>
        @if(Session::has('message'))
        <div class="alert bg-pikachu alert-dismissible col-lg-9 ml-auto p-3 fixed-top">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong class="text-white-50">{{Session::get('message')}}</strong>
        </div>
        @endif
        @yield('content')
    </main>


{{-- Jquery --}}
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>

{{-- SWAL --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
{{-- <script src="sweetalert2.all.min.js"></script> --}}

@include('modal')
</body>
</html>
