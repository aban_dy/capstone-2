@extends('layouts.master')
@section('content')
@include('includes.navbar')

{{--Main --}}
@auth
@if(Auth::user()->role_id == 1)
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-9 ml-auto">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4 py-3">
                    <div class="card py-3 shadow">
                        <div class="card-body">
                            <i class="fas fa-user-friends fa-3x text-success py-2"></i>
                            <h5 class="card-title">Current Working</h5>
                            <p class="card-text h5 text-secondary">
                                {{count($attendances)}}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 py-3">
                    <div class="card py-3 shadow">
                        <div class="card-body">
                            <i class="fas fa-user-tie fa-3x text-warning py-2"></i>
                            <h5 class="card-title">Total Employees</h5>
                            <p class="card-text h5 text-secondary">
                                {{count($users)}}
                            </p>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12 col-md-6 col-lg-4 py-3">
                    <div class="card py-3 shadow">
                        <div class="card-body">
                            <i class="fas fa-clock fa-3x text-danger py-2"></i>
                            <h5 class="card-title">Current Date</h5>
                            <p class="card-text h5 text-secondary">
                                {{Carbon\Carbon::now()->format('Y-m-d')}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 ml-auto mt-3">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-8">
                    <div class="card py-3 shadow w-100">
                        <div class="card-header border-bottom">
                            <span class="card-title text-black-50 h5">
                                <i class="fas fa-calendar-alt text-warning fa-lg p-2"></i>
                                Create Announcements / Events
                            </span>
                            
                        </div>

                        {{-- Card for indiv announcement --}}
                        <div class="card-body">
                            <div class="card">
                                <form class="input-group p-3" id="announce">
                                    {{ csrf_field() }}
                                    <div class="form-group w-100">
                                        <label class="text-black-50">Title</label>
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                    <div class="form-group w-100">
                                        <label class="text-black-50">Body</label>
                                        <input type="text" name="body" class="form-control">
                                    </div>
                                    {{-- Cartegory --}}
                                    <div class="form-group w-100">

                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}">
                                                {{$category->name}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-danger btn-lg align-items-center d-flex justify-content-between">
                                        <span class="px-2 text-uppercase">Post</span>
                                        <i class="fas fa-plus-circle px-2"></i>
                                    </button>
                                </div>    
                            </form>
                            {{-- end for indiv announcement --}}
                        </div>
                    </div>
                </div>

                {{-- Quick Announcement/Events --}}
                <div class="col-sm-12 col-mg-6 col-lg-4" >

                    <div class="card py-3 shadow bg-secondary w-100">
                        <div class="card-header border-bottom text-center">
                            <span class="card-title text-white-50 h5" id="history">
                                <i class="fas fa-calendar-check fa-lg text-warning p-2"></i>
                                History
                            </span>

                        </div>
                        <div class="card-body overflow-auto" style="max-height: 364px; min-height: 364px;">
                            @foreach($announcements as $announcement)
                            <div class="card bg-pikachu mt-2">
                                <div class="card-body text-center">
                                   <img src="https://picsum.photos/200" width="32px" height="32px" class="bg-secondary rounded-circle">
                                   <p class="text-white-50 m-0">
                                    {{$announcement->user->email}}
                                </p>
                                <p class="text-white-50 m-0">{{$announcement->category->name}}</p>
                                <p class="text-white-50 m-0">
                                    {{$announcement->created_at->diffForHumans()}}
                                </p>
                            </div>
                            <div class="card-footer">
                                <form  class="d-none delete" action="/delete/{{$announcement->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button class="btn btn-sm" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteAlert()">
                                    <i class="fas fa-minus-circle fa-lg text-danger"></i>
                                </button>
                                <a href="#" class="rounded-circle" data-toggle="tooltip" data-placement="bottom" title="View">
                                    <i class="fas fa-vector-square fa-lg text-success"></i>
                                </a>
                                
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>    
            </div>
            {{-- end of quick announcemtn/events --}}
        </div>
    </div>
</div>


{{-- Employess' Card --}}
<div class="col-lg-9 ml-auto mt-3">
    <div class="row">

        {{-- Card --}}
        @foreach($users as $user)
        <div class="col-lg-4 mt-3">
            <div class="card shadow">
                <div class="card-body text-center">
                    <img src="{{asset('img/'.$user->imgName)}}" class="rounded-circle text-center" height="48px" width="48px">
                    <span class="card-text text-black-50 d-block">
                        {{$user->email}}
                    </span>
                    <hr class="m-0 py-2">
                    <span class="d-flex justify-content-between">
                        <a href="/profile/{{$user->id}}" class="" data-toggle="tooltip" data-placement="bottom" title="View profile">
                           <i class="fas fa-user-tie text-success"></i>
                       </a>
                    <strong class="text-black-50">{{$user->name}}</strong>
                </span>
            </div>
        </div>
    </div>
    @endforeach
{{-- end of card --}}


</div>
</div>
{{-- end of eployees' card --}}

</div>
</div>



{{-- Employees' View --}}
@else

<div class="col-sm-12 col-mg-6 col-lg-9 ml-auto p-3 shadow-lg" >
    <div class="card py-3 shadow bg-secondary w-100">

        <div class="card-header border-bottom text-center">
            <span class="card-title text-white-50 h5" id="history">
                <i class="fas fa-calendar-check fa-lg text-warning p-2"></i>
                {{ __('Annoucements / Events') }}
            </span>
        </div>

        <div class="card-body vh-100" >
            @foreach($announcements as $announcement)
            <div class="card bg-pikachu mt-2">
                <div class="card-body">

                <div class="d-flex align-items-start">
                    <div class="card-text text-right">
                         <img src="https://picsum.photos/200" width="48px" height="48px" class="bg-secondary rounded-circle">
                         <p class="text-white-50 m-0">
                            {{$announcement->user->email}}
                        </p>
                        <p class="text-white-50 m-0">#{{$announcement->category->name}}</p>
                        <p class="text-white-50 m-0">{{$announcement->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="cart-text px-3 text-capitalize w-100">
                        <strong class="text-white-50 h3 my-5">#{{$announcement->title}}</strong>
                        <p class="text-white-50">{{$announcement->body}}</p>
                    </div>
                </div>    
                

            </div>
        </div>
        @endforeach


    </div>



@endif
@endauth
{{-- end of employees' view --}}


@include('modal')

<script type="text/javascript">
    //Create announcement fetch
    const form = document.getElementById("announce");

    form.addEventListener('submit',(event)=>{
        const title = document.querySelector("#announce input[name = 'title']").value;
        const body = document.querySelector("#announce input[name = 'body']").value;
        const category_id = document.querySelector("#category_id").value;


        event.preventDefault();

        if(title == "" || body == ""){
            Swal.fire({
                icon : 'warning',
                title : 'Enter some text!'
            });
        }else{

            let data = new FormData;
            data.append("_token", "{{csrf_token()}}");
            data.append("title",title);
            data.append("body",body);
            data.append("category_id", category_id);

            fetch("/announce",{
               method: "POST",
               body:data
           }).then((res)=>{

            Swal.fire({
              icon: 'success',
              title: 'Success!'
          }).then(function(isConfirm){
            if (isConfirm) {
                location.reload();
                window.location.href = "#announce";
            }
        });

      });

       }

   });
</script>
{{-- and of main --}}


@endsection


